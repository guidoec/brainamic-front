import React, {Component} from 'react'
import {observer, inject} from 'mobx-react'
import {observe} from 'mobx'
import './LoginContainer.scss'
import * as yup from 'yup'
import * as _ from 'lodash'

// Components
import Button from '@material-ui/core/Button';
import {Formik} from 'formik';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Alert from "../../components/alert-component/AlertComponent";

class LoginContainer extends Component {
    constructor(props) {
        super(props)

        this.formik = React.createRef();

        this.state = {
            initialValues: {
                email: '', password: ''
            },
            genders: {
                masculine: {label: 'Masculino', value: 0},
                feminine: {label: 'Femenino', value: 1}
            },
            formError: {}
        }

        this.disposers = []
        this.disposers.push(
            observe(this.props.userStore.loginState, change => {
                if (change.oldValue === 'pending' && change.newValue === 'done') {
                    this.formik.current.setSubmitting(false)
                }
            }),
            observe(this.props.errorStore.custom, change => {
                if (change.name === 'loginUser') {
                    if (change.newValue !== 500) {
                        this.setState({formError: {message: 'Error en las credenciales enviadas'}})
                    }
                }
            })
        )
    }

    render() {
        const GENDERS = this.state.genders;
        const {formError} = this.state;
        const { history } = this.props;

        return (
            <div className="login-wrapper">
                <div className="login-wrapper__back" onClick={history.goBack}>
                    <ArrowBackIcon/>
                    <Typography variant="body2" component="p">
                        <b>Atras</b>
                    </Typography>
                </div>
                <Paper className="login-wrapper__box" elevation={5}>
                    <div className="login-wrapper__box__head basic-header">
                        <div className="basic-header__wrapper">
                            <Typography variant="headline" component="h3">
                                <b>Login</b>
                            </Typography>
                        </div>
                    </div>
                    <div className="login-wrapper__box__form">
                        {!_.isEmpty(formError) && <Alert type="error" text={formError.message}/>}
                        <Formik
                            ref={this.formik}
                            initialValues={this.state.initialValues}
                            validationSchema={
                                yup.object().shape({
                                    email: yup.string().email().required(),
                                    password: yup.string().required(),
                                })
                            }
                            onSubmit={this.submitForm}
                            render={({
                                         values,
                                         errors,
                                         touched,
                                         handleBlur,
                                         handleChange,
                                         handleSubmit,
                                         isSubmitting,
                                     }) => (
                                <form onSubmit={handleSubmit}>
                                    <TextField
                                        className="login-wrapper__box__form__field"
                                        required
                                        error={errors.email && touched.email}
                                        helperText={touched.email && errors.email}
                                        id="email"
                                        type="email"
                                        name="email"
                                        label="Correo"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.email}
                                    />
                                    <TextField
                                        className="login-wrapper__box__form__field"
                                        required
                                        error={errors.password && touched.password}
                                        helperText={touched.password && errors.password}
                                        type="password"
                                        name="password"
                                        id="password"
                                        label="Contraseña"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.password}
                                    />
                                    <Button variant="raised" color="secondary" type="submit"
                                            disabled={isSubmitting}>
                                        Iniciar sessión
                                    </Button>
                                </form>
                            )}
                        />
                    </div>
                </Paper>
            </div>
        )
    }

    submitForm = (values, actions) => {
        this.props.userStore.login(values.email, values.password)
    }

    componentDidMount() {
        if (this.props.userStore.userData.token.length) this.props.history.push('/app')
    }

    componentWillUnmount() {
        this.disposers.forEach(dispose => dispose())
    }
}

export default inject((all) => ({
    userStore: all.store.user,
    errorStore: all.store.errorStore
}))(observer(LoginContainer))