import React, {Component} from 'react'
import './ServicesModals.scss'
import {FormServices} from '../../../../../services/public/FormServices'
import {inject, observer} from 'mobx-react'
import {withStyles} from '@material-ui/core/styles'
import * as yup from 'yup'
import {PLANS} from "../ServicesContent";

// Components
import Button from '@material-ui/core/Button';
import {Formik} from 'formik';
import TextField from '@material-ui/core/TextField';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import {ExpandMore} from '@material-ui/icons';

const contentStyleOverrides = theme => ({
    firstTitle: {
        fontWeight: 'bold'
    },
    secondTitle: {
        marginLeft: '1rem'
    }
});


export class ModalContent extends Component {
    render() {
        const {classes, modalStore} = this.props;

        return (
            <div className="presential-modal-content">
                <div className="presential-modal-content__info">
                    <Typography variant="body1">
                        {PLANS[modalStore.modalId].summary}
                    </Typography>
                </div>
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMore/>}>
                        <Hidden smDown>
                            <Typography classes={{subheading: classes.firstTitle}} variant="subheading">¿Tienes
                                dudas?</Typography>
                        </Hidden>
                        <Typography classes={{subheading: classes.secondTitle}} variant="subheading">Contacta con
                            nosotros</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className="presential-modal-content__form">
                        <Formik
                            initialValues={{name: '', email: '', comment: ''}}
                            validationSchema={
                                yup.object().shape({
                                    name: yup.string().required(),
                                    email: yup.string().email().required(),
                                    comment: yup.string().required()
                                })
                            }
                            onSubmit={this.sendForm}
                            render={({
                                         values,
                                         errors,
                                         touched,
                                         handleBlur,
                                         handleChange,
                                         handleSubmit,
                                         isSubmitting,
                                     }) => (
                                <form onSubmit={handleSubmit}>
                                    <TextField
                                        className="presential-modal-content__form__field"
                                        required
                                        error={errors.name && touched.name}
                                        id="name"
                                        type="text"
                                        name="name"
                                        label="Nombre"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.name}
                                    />
                                    <TextField
                                        className="presential-modal-content__form__field"
                                        required
                                        error={errors.email && touched.email}
                                        id="email"
                                        type="email"
                                        name="email"
                                        label="Correo"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.email}
                                    />
                                    <TextField
                                        className="presential-modal-content__form__field"
                                        required
                                        error={errors.comment && touched.comment}
                                        type="textarea"
                                        name="comment"
                                        id="comment"
                                        label="Comentario"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.comment}
                                    />
                                    <Button variant="raised" color="secondary" type="submit"
                                            disabled={isSubmitting}>
                                        Enviar
                                    </Button>
                                </form>
                            )}
                        />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        )
    }

    sendForm = (values, actions) => {
        FormServices.moreInfo(values)
            .then(res => {
                actions.setSubmitting(false)
            })
            .catch(err => {
                actions.setSubmitting(false)
            })
    }

    componentDidMount() {
        this.props.modalStore.setModalTitle(PLANS[this.props.modalStore.modalId].title)
    }
}

const actionStyleOverrides = theme => ({
    display1: {
        fontWeight: 'bold'
    }
});


class ModalActions extends Component {
    render() {
        const {classes, modalStore} = this.props;

        return (
            <div className="presential-modal-actions">
                <div className="presential-modal-actions__price">
                    <Typography variant="subheading" color="inherit">
                        Precio plan
                    </Typography>
                    <Typography classes={{display1: classes.display1}} variant="display1" color="inherit">
                        {PLANS[modalStore.modalId].price}€
                    </Typography>
                </div>
                <Button onClick={this.handleClose} color="primary" variant="raised" autoFocus>
                    Contratar
                </Button>
            </div>
        )
    }

    handleClose = () => {
        this.props.userStore.setSelectedPlanId(PLANS[this.props.modalStore.modalId].id);
        this.props.modalStore.setModalOpen(false);
        this.props.history.push('/signup')
    }
}

const PresentialModalActions = withStyles(actionStyleOverrides)(inject(all => ({
    modalStore: all.store.modalStore,
    userStore: all.store.user
}))(observer(ModalActions)));

const PresentialModalContent = withStyles(contentStyleOverrides)(inject(all => ({modalStore: all.store.modalStore}))(observer(ModalContent)));

export {PresentialModalActions, PresentialModalContent};