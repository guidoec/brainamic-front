import {publicHttp} from '../index'

export class FormServices {

    static moreInfo(data) {
        return new Promise((resolve, reject) => {
            publicHttp('mail/contact', 'post', {
                ...data
            })
                .then(response => resolve(response))
                .catch(err => reject(err))
        })
    }

}