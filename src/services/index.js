import axios from 'axios'

export const publicHttpInstance = axios.create({
    baseURL: process.env.REACT_APP_API_URI
});

export function publicHttp(url, method, params = {}) {
    return new Promise((resolve, reject) => {
        publicHttpInstance[method](url, params)
            .then(res => resolve(res))
            .catch(err => {
                console.log('Generic catch', err);
                reject(err)
            });
    })
}

export const privateHttpInstance = axios.create({
    baseURL: process.env.REACT_APP_API_URI
});

export function privateHttp(url, method, params = {}) {
    return new Promise((resolve, reject) => {
        privateHttpInstance[method](url, params)
            .then(res => resolve(res))
            .catch(err => {
                console.log('Generic catch', err);
                reject(err)
            });
    })
}