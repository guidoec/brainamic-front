import { observable, computed, action, get ,set } from "mobx"

export class RouterStore {
    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    route = observable({
        current: ''
    });

    mainPublicPath = computed(() => {
        const PATH_SPLIT = this.route.current.split('/');
        const PATH_ELEMENTS = PATH_SPLIT.filter(Boolean);
        return `/${PATH_ELEMENTS[0]}/${PATH_ELEMENTS[1]}`
    });

    storeRoute = action('storeRoute', pathname => {
        this.route.current = pathname;
    })
}