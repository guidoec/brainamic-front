import React, {Component} from 'react'
import {withStyles} from '@material-ui/core/styles';
import {RouteWithSubRoutes} from '../../router/index'
import {inject, observer} from 'mobx-react'
import {Link} from 'react-router-dom'
// Components
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {
    Menu as MenuIcon
} from '@material-ui/icons';
import {drawerItems} from "./DrawerItems";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';

const styleOverrides = theme => ({
    root: {
        flexGrow: 1,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        minHeight: '100vh'
    },
    appBar: {
        backgroundColor: theme.palette.secondary.dark,
        zIndex: theme.zIndex.drawer + 1,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        minWidth: 0, // So the Typography noWrap works
    },
    drawerPaper: {
        [theme.breakpoints.up('md')]: {
            position: 'relative',
        },
        width: 280,
    },
    toolbar: theme.mixins.toolbar,
    toolbarLeft: {
        display: 'flex',
        alignItems: 'center'
    },
    toolbarOverride: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    link: {
        textDecoration: 'none'
    }
});

const DrawerList = withStyles(styleOverrides)((props) => {
    return (
        <List>
            {Object.keys(drawerItems).map((item, index) => (
                <div key={index}>
                    <Link className={props.classes.link} to={drawerItems[item].path}>
                        <ListItem button>
                            <ListItemIcon>
                                {drawerItems[item].icon}
                            </ListItemIcon>
                            <ListItemText primary={drawerItems[item].label}/>
                        </ListItem>
                    </Link>
                </div>
            ))}
        </List>
    )
});

const AppContainer = class AppContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mobileOpen: false,
            anchorEl: null
        }
    }

    render() {
        const {classes} = this.props;
        const {anchorEl} = this.state;
        return (
            <div className={classes.root}>
                <AppBar position="absolute" className={classes.appBar}>
                    <Toolbar className={classes.toolbarOverride}>
                        <div className={classes.toolbarLeft}>
                            <Hidden mdUp>
                                <IconButton color="inherit"
                                            aria-label="Menu"
                                            onClick={this.toggleDrawer}
                                >
                                    <MenuIcon/>
                                </IconButton>
                            </Hidden>
                            <Typography variant="title" color="inherit" noWrap>
                                Brainamic
                            </Typography>
                        </div>
                        <div>
                            <IconButton onClick={this.openProfileMenu}
                                        aria-owns={anchorEl ? 'simple-menu' : null}
                                        aria-haspopup="true"
                            >
                                <Avatar>G</Avatar>
                            </IconButton>
                            <Menu
                                id="simple-menu"
                                anchorEl={anchorEl}
                                open={Boolean(anchorEl)}
                                onClose={this.closeProfileMenu}
                            >
                                <MenuItem onClick={this.closeProfileMenu}>Profile</MenuItem>
                                <MenuItem onClick={this.closeProfileMenu}>My account</MenuItem>
                                <MenuItem onClick={this.logoutUser}>Cerrar sesión</MenuItem>
                            </Menu>
                        </div>
                    </Toolbar>
                </AppBar>
                <Hidden smDown>
                    <Drawer
                        variant="permanent"
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                    >
                        <div className={classes.toolbar}/>
                        <DrawerList/>
                    </Drawer>
                </Hidden>
                <Hidden mdUp>
                    <Drawer
                        variant="temporary"
                        anchor="left"
                        open={this.state.mobileOpen}
                        onClose={this.toggleDrawer}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        <DrawerList/>
                    </Drawer>
                </Hidden>
                <main className={classes.content}>
                    <div className={classes.toolbar}/>
                    {this.props.routes.map((route, i) => <RouteWithSubRoutes key={i} {...route}/>)}
                </main>
            </div>
        )
    }

    openProfileMenu = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    logoutUser = () => {
        this.closeProfileMenu();
        this.props.userStore.logout();
    }

    closeProfileMenu = () => {
        this.setState({anchorEl: null});
    };

    toggleDrawer = () => {
        this.setState({mobileOpen: !this.state.mobileOpen});
    };
};

export default inject((all) => ({
    userStore: all.store.user
}))(observer(withStyles(styleOverrides)(AppContainer)))