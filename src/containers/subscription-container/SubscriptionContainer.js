// Imports
import React, {Component} from 'react'
import {observer, inject} from 'mobx-react'
import './SubscriptionContainer.scss'
// Components
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';
import {withStyles} from '@material-ui/core/styles';

const stylesOverrides = {
    radio: {
        width: 20,
        height: 20,
        marginRight: 5
    },
    radioChecked: {
        color: 'white !important'
    }
};

class SubscriptionContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            planSelected: null
        }
    }

    render() {
        const {plansStore, classes} = this.props;
        const {planSelected} = this.state;

        return (
            <Grid className="subscription-container" container spacing={24}>
                <Grid item xs={12}>
                    <Typography variant="display1">
                        Planes
                    </Typography>
                    <Typography variant="headline">
                        Subscribete al plan que más se adapte a ti
                    </Typography>
                </Grid>
                <Grid item container spacing={24}>
                    <Grid item xs={12} sm={4}>
                        {plansStore.list.map(plan => (
                            <Card key={plan.id}>
                                <CardContent>
                                    <Typography variant="headline">
                                        {plan.name}
                                    </Typography>
                                    <Typography variant="body2" component="h2">
                                        {plan.description}
                                    </Typography>
                                    <Typography variant="body2">
                                        Frecuencia: {plan.frequency}
                                    </Typography>
                                    <Typography variant="title">
                                        Precio: {plan.amount.value}
                                    </Typography>
                                    <Typography variant="caption">
                                        IVA: {plan.tax.value}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button size="small" variant="contained"
                                            color={planSelected === plan.id ? 'primary' : 'default'}
                                            onClick={() => this.selectPlan(plan.id)}
                                    >
                                        <Radio
                                            checked={planSelected === plan.id}
                                            onChange={() => this.selectPlan(plan.id)}
                                            value={plan.id}
                                            className={classes.radio}
                                            classes={{
                                                checked: classes.radioChecked
                                            }}
                                            name="subscription-plans"
                                            aria-label={plan.name}
                                        />

                                        {planSelected === plan.id ? 'Plan seleccionado' : 'Seleccionar plan'}
                                    </Button>
                                </CardActions>
                            </Card>
                        ))}
                    </Grid>
                </Grid>
            </Grid>
        )
    }

    selectPlan = (planId) => {
        this.setState((prevState, props) => ({
            planSelected: planId
        }))
    };

    createAgreement = ({values, actions}) => {
        this.props.plansStore.createAgreement(values.planSelected)
    };

    componentDidMount() {
        this.props.plansStore.fetchPlansList();
    }
}

export default inject((all) => ({
    plansStore: all.store.plans
}))(observer(withStyles(stylesOverrides)(SubscriptionContainer)));