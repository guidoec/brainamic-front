import React, {Component} from 'react'
import trainingImg from '../../../../assets/img/services-image-1.jpg'
import './ServiceTraining.scss'
import {inject, observer} from 'mobx-react'
import {PLANS} from './ServicesContent'
// Components
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {PresentialModalContent, PresentialModalActions} from './modal-contents/ServicesModals'

class ServiceTraining extends Component {

    render() {
        return (
            <div className="service-training-wrapper">
                <div className="presential-training-wrapper">
                    <div className="presential-training-wrapper__title">
                        <Typography variant="title">
                            Entrenamiento Presencial
                            <hr/>
                        </Typography>
                    </div>
                    <div className="presential-training-wrapper__card">
                        <Card className="presential-card">
                            <CardMedia
                                image={trainingImg}
                                title="Contemplative Reptile"
                                className="presential-card__media"
                            />
                            <CardContent className="presential-card__content">
                                <Typography gutterBottom variant="headline" component="h2">
                                    {PLANS.presential.title}
                                </Typography>
                                <Typography component="p">
                                    {PLANS.presential.summary}
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Button size="small" color="primary"
                                        onClick={() => this.loadModalAndOpen('presential')}>
                                    Más información
                                </Button>
                            </CardActions>
                        </Card>
                    </div>
                </div>
                <div className="online-training-wrapper">
                    <div className="online-training-wrapper__title">
                        <Typography variant="title">
                            Entrenamiento Online
                            <hr/>
                        </Typography>
                    </div>
                    <div className="online-training-wrapper__cards">
                        {Object.keys(PLANS).map((planKey, key) => (key > 0 &&
                            <Card className="online-card" key={key}>
                                <CardMedia
                                    image={trainingImg}
                                    title="Contemplative Reptile"
                                    className="online-card__media"
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="headline" component="h2">
                                        {PLANS[planKey].title}
                                    </Typography>
                                    <Typography component="p">
                                        {PLANS[planKey].summary}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button size="small" color="primary"
                                            onClick={() => this.loadModalAndOpen(PLANS[planKey].id)}>
                                        Más información
                                    </Button>
                                </CardActions>
                            </Card>
                        ))}
                    </div>
                </div>
            </div>
        )
    }

    loadModalAndOpen = (id) => {
        this.props.modalStore.setModalId(id);
        this.props.modalStore.setModalContent(PresentialModalContent);
        this.props.modalStore.setModalActions(PresentialModalActions);
        this.props.modalStore.setModalOpen(true);
    }
}

export default inject(all => ({
    modalStore: all.store.modalStore
}))(observer(ServiceTraining));