import {
    Note as IconNote,
    Group as IconGroup,
    Chat as IconChat,
    Today as IconToday,
    FitnessCenter as IconTraining,
    PlayArrow as IconMultimedia,
    RssFeed as IconAdvisory,
    Restaurant as IconNutrition,
    Poll as IconProgress,
    ZoomOutMap as IconMultidisciplinary,
    LocalOffer as IconSubscription,
} from '@material-ui/icons';
import React from 'react'

export const drawerItems = {
    initialDocuments: {
        path: '/app/documents',
        icon: <IconNote/>,
        label: 'Documentos Iniciales'
    },
    community: {
        path: '/app/community',
        icon: <IconGroup/>,
        label: 'Comunidad'
    },
    chat: {
        path: '/app/chat',
        icon: <IconChat/>,
        label: 'Chat'
    },
    agenda: {
        path: '/app/agenda',
        icon: <IconToday/>,
        label: 'Agenda'
    },
    training: {
        path: '/app/training',
        icon: <IconTraining/>,
        label: 'Entrenamiento'
    },
    multimedia: {
        path: '/app/multimedia',
        icon: <IconMultimedia/>,
        label: 'Multimedia'
    },
    advisory: {
        path: '/app/advisory',
        icon: <IconAdvisory/>,
        label: 'Asesoria'
    },
    nutrition: {
        path: '/app/nutrition',
        icon: <IconNutrition/>,
        label: 'Nutrición'
    },
    progress: {
        path: '/app/progress',
        icon: <IconProgress/>,
        label: 'Progreso'
    },
    multidisciplinary: {
        path: '/app/multidisciplinary',
        icon: <IconMultidisciplinary/>,
        label: 'Multidisciplinario'
    },
    subscription: {
        path: '/app/subscription',
        icon: <IconSubscription/>,
        label: 'Subscripción'
    }
};