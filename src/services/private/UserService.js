import {publicHttp, privateHttp} from '../index'

export class UserService {

    static async signup(data) {
        return await publicHttp('users', 'post', {
            ...data
        })
    }

    static async login(email, password) {
        return await publicHttp('login', 'post', {
            email, password
        })
    }

    static async logout(email, password) {
        return await privateHttp('logout', 'get', {
            email, password
        })
    }

}