import {privateHttp} from '../index'

export class PlansService {

    /**
     * Fetch list of paypal plans
     * @return
     */
    static async fetchPlansList() {
        return await privateHttp('paypal/plans', 'get')
    }

    /**
     * Fetch plan detail by id
     * @param planId
     * @returns {Promise.<*>}
     */
    static async fetchPlanDetail(planId) {
        return await privateHttp(`paypal/plans/${planId}`, 'get')
    }

    /**
     * Create plan agreement
     * @param planId
     * @returns {Promise.<*>}
     */
    static async createAgreement(planId) {
        return await privateHttp(`paypal/plans/agreement`, 'post', {planId})
    }

}