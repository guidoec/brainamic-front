export const PLANS = {
    presential: {
        id: 'presential',
        title: 'Plan Presencial Madrid',
        summary: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at vehicula elit. Suspendisse potenti.
         Quisque ultrices dapibus sem id fermentum. Vivamus interdum sem eget est dignissim, aliquet mollis elit efficitur. 
         Vivamus congue risus quis lorem vestibulum, sodales aliquet sem faucibus. Pellentesque habitant morbi tristique
         senectus et netus et malesuada fames ac turpis egestas. In blandit neque a est efficitur vestibulum. Cras vitae vulputate dolor.`,
        price: 90
    },
    economy: {
        id: 'economy',
        title: 'Plan Economy',
        summary: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at vehicula elit. Suspendisse potenti.
         Quisque ultrices dapibus sem id fermentum. Vivamus interdum sem eget est dignissim, aliquet mollis elit efficitur. 
         Vivamus congue risus quis lorem vestibulum, sodales aliquet sem faucibus. Pellentesque habitant morbi tristique
         senectus et netus et malesuada fames ac turpis egestas. In blandit neque a est efficitur vestibulum. Cras vitae vulputate dolor.`,
        price: 50
    },
    fitness: {
        id: 'fitness',
        title: 'Plan Fitness',
        summary: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at vehicula elit. Suspendisse potenti.
         Quisque ultrices dapibus sem id fermentum. Vivamus interdum sem eget est dignissim, aliquet mollis elit efficitur. 
         Vivamus congue risus quis lorem vestibulum, sodales aliquet sem faucibus. Pellentesque habitant morbi tristique
         senectus et netus et malesuada fames ac turpis egestas. In blandit neque a est efficitur vestibulum. Cras vitae vulputate dolor.`,
        price: 70
    },
    concurrent: {
        id: 'concurrent',
        title: 'Plan Concurrente',
        summary: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at vehicula elit. Suspendisse potenti.
         Quisque ultrices dapibus sem id fermentum. Vivamus interdum sem eget est dignissim, aliquet mollis elit efficitur. 
         Vivamus congue risus quis lorem vestibulum, sodales aliquet sem faucibus. Pellentesque habitant morbi tristique
         senectus et netus et malesuada fames ac turpis egestas. In blandit neque a est efficitur vestibulum. Cras vitae vulputate dolor.`,
        price: 80
    }
};