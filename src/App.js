import React, {Component} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import {inject, observer} from 'mobx-react'
// import {BrowserRouter as Router} from 'react-router-dom';
import {Router} from 'react-router-dom';
import history from './router/history'
import {routes, RouteWithSubRoutes} from './router/index'
import {Provider} from 'mobx-react'
import {rootStore} from "./store/index";
import './App.scss';
import "slick-carousel/slick/slick.scss";
import "slick-carousel/slick/slick-theme.scss";
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import MainModal from './components/main-modal/MainModal'
import MainLoader from './components/main-loader/MainLoader'

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#8d8d8d',
            main: '#606060',
            dark: '#363636',
            contrastText: '#ffffff',
        },
        secondary: {
            light: '#ff6659',
            main: '#d32f2f',
            dark: '#9a0007',
            contrastText: '#ffffff',
        },
    },
});

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            store: rootStore
        };

        this.createLocalStorage();
    }

    render() {
        const {store} = this.state;
        return (
            <React.Fragment>
                <CssBaseline/>
                <MuiThemeProvider theme={theme}>
                    <Provider store={store}>
                        <AppCoreComponent/>
                    </Provider>
                </MuiThemeProvider>
            </React.Fragment>
        );
    }

    createLocalStorage = () => {
        if (!window.localStorage.hasOwnProperty('brainamic')) {
            window.localStorage.setItem('brainamic', '{}');
        }
    }
}

class AppCore extends Component {

    render() {
        const {store} = this.props;

        return (
            <div>
                <Router history={history}>
                    <div>
                        {routes.map((route, i) => <RouteWithSubRoutes key={i} {...route}/>)}
                        <MainModal/>
                        {store.ui.mainLoader.active && <MainLoader/>}
                    </div>
                </Router>
            </div>
        )
    }
}

const AppCoreComponent = inject('store')(observer(AppCore));

export default App;
