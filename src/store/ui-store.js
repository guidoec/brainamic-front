import {observable, computed, action, get, set} from "mobx"

export class UiStore {
    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    mainLoader = observable({
        active: false
    })

    setMainLoaderStatus = action('setMainLoaderStatus', (bool) => {
        this.mainLoader.active = bool
    })
}