import {UserStore} from "./user-store";
import {RouterStore} from "./router-store";
import {ModalStore} from "./modal-store";
import {ErrorStore} from "./error-store";
import {PlansStore} from "./plans-store";
import {UiStore} from "./ui-store";

class RootStore {
    constructor() {
        this.user = new UserStore(this);
        this.errorStore = new ErrorStore(this);
        this.routerStore = new RouterStore(this);
        this.modalStore = new ModalStore(this);
        this.plans = new PlansStore(this);
        this.ui = new UiStore(this)
    }
}

export const rootStore = new RootStore();