import React, {Component} from 'react'
import './MainLoader.scss'
import {inject, observer} from 'mobx-react'

// Components
import CircularProgress from '@material-ui/core/CircularProgress';
import Fade from '@material-ui/core/Fade';

class MainLoader extends Component {

    render() {
        const {uiStore} = this.props;

        return (
            <Fade in={uiStore.mainLoader.active} timeout={500}>
                <div className="main-loader-container">
                    <CircularProgress size={70} className="main-loader-container__progress"/>
                </div>
            </Fade>
        )
    }
}

export default inject(all => ({
    uiStore: all.store.ui
}))(observer(MainLoader));