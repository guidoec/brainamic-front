import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import {inject, observer} from 'mobx-react'
// Components
import Dialog from '@material-ui/core/Dialog';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {withStyles} from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const styleOverrides = {
    dialogCloseIcon: {
        position: 'absolute',
        right: 0,
        top: 0,
        color: '#CCCCCC'
    }
};

class MainModal extends Component {
    render() {
        const {fullScreen, modalStore, classes, ...rest} = this.props;
        const ModalContent = modalStore.modal.contentComponent;
        const ModalActions = modalStore.modal.actionsComponent;

        return (
            <Dialog
                fullScreen={fullScreen}
                maxWidth={false}
                open={modalStore.modal.opened}
                onBackdropClick={this.handleClose}
                aria-labelledby="responsive-dialog-title"
                history={this.props.history}
            >
                <IconButton className={classes.dialogCloseIcon} color="inherit" aria-label="Close">
                    <CloseIcon onClick={this.handleClose}/>
                </IconButton>
                <DialogTitle id="responsive-dialog-title">
                    {modalStore.modal.title}
                </DialogTitle>
                <DialogContent>
                    {ModalContent.length && <ModalContent {...rest}/>}
                </DialogContent>
                <DialogActions>
                    {ModalActions.length && <ModalActions {...rest}/>}
                </DialogActions>
            </Dialog>
        )
    }

    navigateTo = (route) => {
        console.log(route)
    }

    handleClose = () => {
        this.props.modalStore.setModalOpen(false);
        setTimeout(() => this.props.modalStore.resetModal(), 150)
    }
}

export default withRouter(withMobileDialog()(withStyles(styleOverrides)(inject(all => ({modalStore: all.store.modalStore}))(observer(MainModal)))));