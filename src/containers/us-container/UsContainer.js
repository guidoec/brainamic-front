import React, {Component} from 'react';
import './UsContainer.scss';

// Slider imgs
import image1 from '../../assets/img/header-banner-1.jpg'
import image2 from '../../assets/img/services-image-1.jpg'

import brainamicLogo from '../../assets/img/brainamic-logo.png'

// Components
import Slider from "react-slick";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography'

class UsContainer extends Component {
    constructor(props) {
        super(props)

        this.sliderSettings = {
            arrows: false,
            autoplay: true,
            mobileFirst: true,
            autoplaySpeed: 5000,
            fade: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };
        this.sliderImages = [image1, image2]
    }

    render() {
        return (
            <div className="us-container">
                <div className='us-container__slider'>
                    <div className="us-container__slider__claim">
                        <img className="company-logo" src={brainamicLogo} alt=""/>
                        <Typography variant="display2" component="h2">
                            ¡Bienvenidos a Brainamic!
                        </Typography>
                        <Typography variant="subheading" component="p">
                            En esta plataforma podrás encontrar servicios de entrenamiento, fisioterapia y nutrición.
                            Para ayudarte en el proceso tendrás a tu disposición vídeos explicativos, comunidad de dudas, chat con el equipo, así como un seguimiento totalmente personalizado.
                            Además, tenemos diferentes packs para que elijas el que más se adapta a tus necesidades (fitness, oposiciones, deportistas, etc.).
                            Un equipo multidisciplinar te guiará de forma conjunta hacia tus objetivos de la forma más eficiente posible (entrenadores, readaptadores, fisioterapeutas, nutricionistas, etc.).
                        </Typography>
                    </div>
                    <Slider { ...this.sliderSettings }>
                        {this.sliderImages.map((img,index)=> (
                            <div className="image-wrapper" key={index}>
                                <img src={img} alt=""/>
                            </div>
                        ))}
                    </Slider>
                </div>
                <Grid container>

                </Grid>
            </div>
        )
    }
}

export default UsContainer;
