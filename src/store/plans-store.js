import {observable, computed, action, get, set} from "mobx"
import {PlansService} from "../services/private/PlansService";
import history from '../router/history'

export class PlansStore {
    constructor(rootStore) {
        this.root = rootStore;
    }

    list = observable.array([], {deep: false})

    fetchPlansList = action('fetchPlanList', () => {
        this.root.ui.setMainLoaderStatus(true);
        PlansService.fetchPlansList()
            .then(({data}) => {
                let detailPlanPromises = [];
                data.plans.forEach(plan => {
                    detailPlanPromises.push(this.fetchPlanDetail(plan.id))
                })

                Promise.all(detailPlanPromises)
                    .then(values => {
                        this.list.replace(
                            values.map(val => {
                                return {
                                    id: val.id,
                                    name: val.name,
                                    description: val.description,
                                    amount: val.payment_definitions[0].amount,
                                    tax: val.payment_definitions[0].charge_models[0].amount,
                                    frequency: val.payment_definitions[0].frequency
                                }
                            })
                        )
                    })
                    .finally(() => {
                        this.root.ui.setMainLoaderStatus(false)
                    })
            })
    })

    fetchPlanDetail = action('fetchPlanDetail', (planId) => {
        return new Promise((resolve, reject) => {
            PlansService.fetchPlanDetail(planId)
                .then(({data}) => {
                    resolve(data)
                })
        })
    })

    createAgreement = action('createAgreement', (planId) => {
        this.root.ui.setMainLoaderStatus(true);
        PlansService.createAgreement(planId)
            .then(({data}) => {
                this.root.ui.setMainLoaderStatus(false);
                window.location.replace(data.links[0].href)
            })
    })
}