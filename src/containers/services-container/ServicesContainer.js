import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import './ServiceContainer.scss'
import {RouteWithSubRoutes} from '../../router/index'
import {Link} from 'react-router-dom'
// Components
import Typography from '@material-ui/core/Typography';
import TransparentButton from '../../components/transparent-button/TransparentButton';

const styleOverrides = theme => ({
    headerTitle: {
        color: theme.palette.primary.contrastText,
        fontWeight: 600,
        alignSelf: 'self-start'
    }
});

const ServicesContainer = class ServicesContainer extends Component {
    render() {
        const {classes} = this.props;

        return (
            <div className="service-container">
                <div className="service-container__header basic-header full-width">
                    <div className="basic-header__wrapper">
                        <Typography variant="headline"
                                    classes={{root: classes.headerTitle}}>
                            Nuestros servicio
                        </Typography>
                        <div className="service-container__header__navigation-buttons">
                            <Link to="/public/services/training">
                                <TransparentButton selected={this.serviceSelected('/public/services/training')}>
                                    Entrenamiento
                                </TransparentButton>
                            </Link>
                            <Link to="/public/services/readaption">
                                <TransparentButton selected={this.serviceSelected('/public/services/readaption')}>
                                    Readptación
                                </TransparentButton>
                            </Link>
                            <Link to="/public/services/others">
                                <TransparentButton selected={this.serviceSelected('/public/services/others')}>
                                    Otros servicios
                                </TransparentButton>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="service-container__content">
                    {this.props.routes.map((route, i) => <RouteWithSubRoutes key={i} {...route}/>)}
                </div>
            </div>
        );
    }

    serviceSelected = (route) => {
        return this.props.location.pathname === route
    }
}

export default withStyles(styleOverrides)(ServicesContainer);
