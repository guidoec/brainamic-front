import React, {Component} from 'react'
import {withStyles} from '@material-ui/core/styles'
// Components
import Button from '@material-ui/core/Button';


const styleOverrides = theme => ({
    transparent: {
        'background-color': 'rgba(255,255,255,.5)'
    }
})

const TransparentButton = class TransparentButton extends Component {
    render() {
        const {classes, selected, ...rest} = this.props;

        return (
            <Button classes={{root: classes.transparent}}
                    style={{backgroundColor: selected ? 'white' : null}}
                    {...rest}/>
        )
    }
}

export default withStyles(styleOverrides)(TransparentButton)