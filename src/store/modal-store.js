import { observable, computed, action, get ,set, autorun } from "mobx"

export class ModalStore {
    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    modal = observable({
        opened: false,
        title: '',
        actionsComponent: '',
        contentComponent: ''
    });

    modalId = observable.box('');

    setModalId = action('setModalId', id => {
      this.modalId = id;
    })

    setModalOpen = action('setModalOpen', opened => {
        this.modal.opened = opened;
    });

    setModalContent = action('setModalContent', component => {
        this.modal.contentComponent = component;
    });

    setModalActions = action('setModalActions', component => {
        this.modal.actionsComponent = component;
    });

    setModalTitle = action('setModalTitle', title => {
        this.modal.title = title;
    });

    resetModal = action('resetModal', () => {
        this.modal.title = '';
        this.modal.actionsComponent = '';
        this.modal.contentComponent = ''
    })
}