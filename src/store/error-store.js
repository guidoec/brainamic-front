import {observable, computed, action, get, set} from "mobx"

export class ErrorStore {
    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    general = observable({
        message: '',
        code: ''
    });

    custom = observable.map()

}