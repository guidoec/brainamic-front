import {observable, computed, action, get, set} from "mobx"
import {privateHttpInstance} from '../services'
import {UserService} from "../services/private/UserService";
import history from '../router/history'

export class UserStore {
    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    userData = observable({
        name: '',
        surname: '',
        email: '',
        username: '',
        token: ''
    });

    userServices = observable(['subscription']);

    selectedPlanId = observable.box('');
    signUpState = observable.box('done');
    loginState = observable.box('done');

    setSelectedPlanId = action('setSelectedPlanId', (id) => {
        this.selectedPlanId.set(id);
    });

    signUp = action('signUpUser', (values) => {
        this.signUpState.set('pending');
        UserService.signup(values)
            .then(res => {
                this.signUpState.set('done');
                this.login(values.email, values.password, true)
            })
            .catch(err => {
                this.rootStore.errorStore.custom.set('signUpUser', err.response.data);
                this.signUpState.set('done');
            })
    });

    login = action('loginUser', (email, password, fromSignUp = false) => {
        this.loginState.set('pending');
        UserService.login(email, password)
            .then(res => {
                this.loginState.set('done');
                const {name, surname, username, token, email, services} = res.data;
                this.userData = {name, username, token, surname, email};
                // this.userServices = [...services];
                privateHttpInstance.defaults.headers.common['Authorization'] = `Bearer ${res.data.token}`;
                fromSignUp ? history.push('/app/subscription') : history.push('/app')
            })
            .catch(err => {
                this.loginState.set('done');
                this.rootStore.errorStore.custom.set('loginUser', err.response.status);
            })
    });

    logout = action('logoutUser', () => {
        UserService.logout()
            .then(res => {
                this.userData.token = '';
                this.userData.name = '';
                this.userData.surname = '';
                this.userData.username = '';
                this.userData.email = '';
                privateHttpInstance.defaults.headers.common['Authorization'] = ''
                history.push('/public')
            })
            .catch(err => {
                this.rootStore.errorStore.custom.set('logoutUser', err.response.data);
            })
    })
}