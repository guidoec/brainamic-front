import React, {Component} from 'react';
import './AlertComponent.scss';
// Components
import Paper from '@material-ui/core/Paper';
import {
    Error as ErrorIcon,
    Warning as WarningIcon,
    Notifications as InfoIcon,
    ThumbUp as SuccessIcon
} from '@material-ui/icons';
import Typography from '@material-ui/core/Typography';


const AlertComponent = class AlertComponent extends Component {
    render() {
        return (
            <Paper className={`alert-wrapper ${this.typeClass()}`}>
                <div className='alert-wrapper__icon'>
                    {this.typeIcon()}
                </div>
                <div className="alert-wrapper__text">
                    <Typography variant="body2">
                        {this.props.text}
                    </Typography>
                </div>
            </Paper>
        )
    }

    typeClass = () => {
        switch (this.props.type) {
            case 'error':
                return 'is-error';
                break;
            case 'warning':
                return 'is-warning';
                break;
            case 'success':
                return 'is-success';
                break;
            case 'info':
                return 'is-info';
                break;
            default:
                break;
        }
    }

    typeIcon() {
        switch (this.props.type) {
            case 'error':
                return (<ErrorIcon/>);
                break;
            case 'warning':
                return (<WarningIcon/>);
                break;
            case 'success':
                return (<SuccessIcon/>);
                break;
            case 'info':
                return (<InfoIcon/>);
                break;
            default:
                break;
        }
    }
}

export default AlertComponent