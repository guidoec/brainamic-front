import React, {Component} from 'react';
import {observer, inject} from 'mobx-react'
import {computed, observe} from 'mobx'
import {withRouter} from 'react-router-dom';
import {RouteWithSubRoutes} from '../../router/index'
import {withStyles} from '@material-ui/core/styles';
// Styles
import './MainContainer.scss'
// Images
import brainamicLogo from '../../assets/img/brainamic-logo.png'
import brainamicLogoBlack from '../../assets/img/brainamic-logo-black.png'
// Components
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Hidden from '@material-ui/core/Hidden'
import Toolbar from '@material-ui/core/Toolbar'
import Drawer from '@material-ui/core/Drawer'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography'

const styleOverrides = theme => ({
    indicator: {
        height: 5,
    },
    tab: {
        height: '95px'
    },
    appBarRoot: {
        display: 'flex',
        'flex-direction': 'row',
        'align-items': 'stretch',
        'justify-content': 'space-between',
        'background-color': theme.palette.primary.dark
    }
});

const MainContainer = class MainContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentRoute: false,
            openedDrawer: false
        };

        this.disposers = [];
        this.disposers.push(
            observe(this.props.store.routerStore.mainPublicPath, (change) => {
                if (!change.newValue.includes('undefined')) {
                    this.setState({currentRoute: change.newValue});
                }
            })
        )
    }

    render() {
        const {classes} = this.props;
        const links = [{
            label: 'Quiénes somos',
            path: '/public/us'
        }, {
            label: 'Servicios',
            path: '/public/services'
        }, {
            label: 'Zona Clientes',
            path: '/login'
        }, {
            label: 'Contacto',
            path: '/public/contact'
        }];

        return (
            <div className={'main-container'}>
                <AppBar className={'main-container__app-bar'}
                        position="fixed" classes={{root: classes.appBarRoot}}>
                    <Hidden smDown>
                        <div className={'main-container__app-bar__logo'}>
                            <img src={brainamicLogo} alt=""/>
                        </div>
                        <Tabs className={'main-container__app-bar__tabs'}
                              centered classes={{indicator: classes.indicator}}
                              onChange={this.handleChange}
                              value={this.state.currentRoute}>
                            {links.map((link, key) => (
                                <Tab classes={{root: classes.tab}} key={key} label={link.label} value={link.path}/>
                            ))}
                        </Tabs>
                    </Hidden>
                    <Hidden mdUp>
                        <Toolbar>
                            <IconButton color="inherit"
                                        aria-label="Menu"
                                        onClick={this.toggleDrawer}
                            >
                                <MenuIcon/>
                            </IconButton>
                            <Typography className={'mobile-isotype'} variant="headline" component="h3">
                                Brainamic
                            </Typography>
                        </Toolbar>
                        <Drawer open={this.state.openedDrawer} onClose={this.toggleDrawer}>
                            <div className={'main-container__app-bar__logo'}>
                                <img src={brainamicLogoBlack} alt=""/>
                            </div>
                            <List>
                                {links.map((link, key) => (
                                    <ListItem button key={key} onClick={() => this.navigateAndClose(link.path)}>
                                        <ListItemText primary={link.label}/>
                                    </ListItem>
                                ))}
                            </List>
                        </Drawer>
                    </Hidden>
                </AppBar>
                <div className="main-container__router-wrapper">
                    {this.props.routes.map((route, i) => <RouteWithSubRoutes key={i} {...route}/>)}
                </div>
            </div>
        );
    }

    toggleDrawer = () => {
        this.setState((prevState, props) => ({
            openedDrawer: !prevState.openedDrawer
        }))
    };

    handleChange = ($event, value) => {
        this.props.history.push(value);
        this.props.store.routerStore.storeRoute(value);
    };

    navigateAndClose = (path) => {
        this.toggleDrawer();
        this.handleChange(null, path);
    };

    componentDidMount() {
        let {location} = this.props;
        if (location.pathname) {
            const validPathname = location.pathname !== window.location.pathname ? window.location.pathname : location.pathname;
            this.props.store.routerStore.storeRoute(validPathname);
        }
    }

    componentWillUnmount() {
        this.disposers.forEach(dispose => dispose())
    }

};


export default withRouter(inject('store')(observer(withStyles(styleOverrides)(MainContainer))));
