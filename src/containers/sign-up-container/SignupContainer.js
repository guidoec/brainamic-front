import React, {Component} from 'react'
import {observer, inject} from 'mobx-react'
import {observe} from 'mobx'
import './SignupContainer.scss'
import * as yup from 'yup'
import * as _ from 'lodash'
import {PLANS} from "../services-container/service-content/service-training/ServicesContent";

// Components
import Button from '@material-ui/core/Button';
import {Formik} from 'formik';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Alert from "../../components/alert-component/AlertComponent";

class SignupContainer extends Component {
    constructor(props) {
        super(props);

        this.formik = React.createRef();

        this.state = {
            initialValues: {
                username: '', email: '', password: '', name: '', surname: '', gender: ''
            },
            genders: {
                masculine: {label: 'Masculino', value: 0},
                feminine: {label: 'Femenino', value: 1}
            },
            formError: {},
            validation: yup.object().shape({
                username: yup.string().matches(/^\S*$/, {message: 'No puede incluir espacios en blanco'}).required(),
                email: yup.string().email().required(),
                password: yup.string().required(),
                name: yup.string(),
                surname: yup.string(),
                gender: yup.boolean().required()
            })
        };

        this.disposers = [];
        this.disposers.push(
            observe(this.props.userStore.signUpState, change => {
                if (change.oldValue === 'pending' && change.newValue === 'done') {
                    this.formik.current.setSubmitting(false)
                }
            }),
            observe(this.props.errorStore.custom, change => {
                if (change.name === 'signUpUser') {
                    this.setState({formError: change.newValue})
                }
            })
        )
    }

    render() {
        const GENDERS = this.state.genders;
        const {formError} = this.state;
        const {userStore, history} = this.props;

        return (
            <div className="signup-wrapper">
                <div className="signup-wrapper__back" onClick={history.goBack}>
                    <ArrowBackIcon/>
                    <Typography variant="body2" component="p">
                        <b>Atras</b>
                    </Typography>
                </div>
                <Paper className="signup-wrapper__box" elevation={5}>
                    <div className="signup-wrapper__box__head">
                        {!_.isEmpty(formError) && <Alert type="error" text={formError.message}/>}
                        <Typography variant="headline">
                            <b>Creación de usuario</b>
                        </Typography>
                        <Typography variant="body1" component="p">
                            Complete los campos obligatorios (*) y a continuación podrá contratar el
                            {userStore.selectedPlanId.get().length ? (<b> {PLANS[userStore.selectedPlanId.get()].title}</b>) : ` plan en el que se
                            ha interesado`}. El registro no implica un compromiso.
                        </Typography>
                    </div>
                    <div className="signup-wrapper__box__form">
                        <Formik
                            ref={this.formik}
                            initialValues={this.state.initialValues}
                            validationSchema={this.state.validation}
                            onSubmit={this.submitForm}
                            render={({
                                         values,
                                         errors,
                                         touched,
                                         handleBlur,
                                         handleChange,
                                         handleSubmit,
                                         isSubmitting,
                                     }) => (
                                <form onSubmit={handleSubmit}>
                                    <TextField
                                        className="signup-wrapper__box__form__field"
                                        required
                                        error={errors.username && touched.username}
                                        helperText={touched.username && errors.username}
                                        id="username"
                                        type="text"
                                        name="username"
                                        label="Nombre de usuario"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.username}
                                    />
                                    <TextField
                                        className="signup-wrapper__box__form__field"
                                        required
                                        error={errors.email && touched.email}
                                        helperText={touched.email && errors.email}
                                        id="email"
                                        type="email"
                                        name="email"
                                        label="Correo"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.email}
                                    />
                                    <TextField
                                        className="signup-wrapper__box__form__field"
                                        required
                                        error={errors.password && touched.password}
                                        helperText={touched.password && errors.password}
                                        type="password"
                                        name="password"
                                        id="password"
                                        label="Contraseña"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.password}
                                    />
                                    <TextField
                                        className="signup-wrapper__box__form__field"
                                        error={errors.name && touched.name}
                                        helperText={touched.name && errors.name}
                                        id="name"
                                        type="text"
                                        name="name"
                                        label="Nombre"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.name}
                                    />
                                    <TextField
                                        className="signup-wrapper__box__form__field"
                                        error={errors.surname && touched.surname}
                                        helperText={touched.surname && errors.surname}
                                        id="surname"
                                        type="text"
                                        name="surname"
                                        label="Apellido"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.surname}
                                    />
                                    <TextField
                                        select
                                        label="Género"
                                        helperText="Seleccione su género"
                                        margin="normal"
                                        className="signup-wrapper__box__form__field"
                                        error={errors.gender && touched.gender}
                                        id="gender"
                                        name="gender"
                                        onChange={handleChange}
                                        value={values.gender}
                                    >
                                        {Object.keys(GENDERS).map(g => (
                                            <MenuItem key={GENDERS[g].value} value={GENDERS[g].value}>
                                                {GENDERS[g].label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    <Button variant="raised" color="secondary" type="submit"
                                            disabled={isSubmitting}>
                                        Crear usuario
                                    </Button>
                                </form>
                            )}
                        />
                    </div>
                </Paper>
            </div>
        )
    }

    submitForm = (values, actions) => {
        this.props.userStore.signUp(values)
    };

    componentWillUnmount() {
        this.disposers.forEach(dispose => dispose())
    }
}

export default inject((all) => ({
    userStore: all.store.user,
    errorStore: all.store.errorStore
}))(observer(SignupContainer))