import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import {rootStore} from '../store/index';
import MainContainer from '../containers/main-container/MainContainer'
import AppContainer from '../containers/app-container/AppContainer'
import ServicesContainer from '../containers/services-container/ServicesContainer'
import SignupContainer from '../containers/sign-up-container/SignupContainer'
import UsContainer from '../containers/us-container/UsContainer'
import ServiceTraining from '../containers/services-container/service-content/service-training/ServiceTraining';
import ServiceReadaption from '../containers/services-container/service-content/service-readaption/ServiceReadaption';
import ServiceOthers from "../containers/services-container/service-content/service-others/ServiceOthers";
import LoginContainer from "../containers/login-container/LoginContainer";
import SubscriptionContainer from '../containers/subscription-container/SubscriptionContainer'
import NoServiceContainer from '../containers/no-service-container/NoServiceContainer'

export const routes = [
    {
        path: '/',
        redirect: '/public/us'
    },
    {
        component: MainContainer,
        path: '/public',
        routes: [
            {
                path: '/',
                redirect: '/public/us'
            },
            {
                path: '/public/us',
                component: UsContainer
            },
            {
                path: '/public/services',
                component: ServicesContainer,
                routes: [
                    {
                        path: '/public/services/',
                        redirect: '/public/services/training'
                    },
                    {
                        path: '/public/services/training',
                        component: ServiceTraining
                    },
                    {
                        path: '/public/services/readaption',
                        component: ServiceReadaption
                    },
                    {
                        path: '/public/services/others',
                        component: ServiceOthers
                    }
                ]
            },
            {
                path: '/public/contact',
                // component: ContactContainer
            }
        ]
    },
    {
        path: '/signup',
        component: SignupContainer
    },
    {
        path: '/login',
        component: LoginContainer
    },
    {
        path: '/app',
        component: AppContainer,
        protected: 'private',
        routes: [
            {
                path: '/app/documents',
                service: 'documents',
                protected: 'service'
                // component: DocsContainer
            },
            {
                path: '/app/community',
                service: 'community',
                protected: 'service'
                // component: CommunityContainer
            },
            {
                path: '/app/chat',
                service: 'chat',
                protected: 'service'
                // component: DocsContainer
            },
            {
                path: '/app/agenda',
                service: 'agenda',
                protected: 'service',
                // component: CommunityContainer
            },
            {
                path: '/app/training',
                service: 'training',
                protected: 'service',
                // component: DocsContainer
            },
            {
                path: '/app/multimedia',
                service: 'multimedia',
                protected: 'service',
                // component: CommunityContainer
            },
            {
                path: '/app/advisory',
                service: 'advisory',
                protected: 'service',
                // component: DocsContainer
            },
            {
                path: '/app/nutrition',
                service: 'nutrition',
                protected: 'service',
                // component: CommunityContainer
            },
            {
                path: '/app/progress',
                service: 'progress',
                protected: 'service',
                // component: DocsContainer
            },
            {
                path: '/app/multidisciplinary',
                service: 'multidisciplinary',
                protected: 'service',
                // component: CommunityContainer
            },
            {
                path: '/app/subscription',
                service: 'subscription',
                protected: 'service',
                component: SubscriptionContainer
            }
        ]
    }
];


// wrap <Route> and use this everywhere instead, then when
// sub routes are added to any route it'll work
export const RouteWithSubRoutes = route => (
    <Route
        exact={route.hasOwnProperty('redirect')}
        path={route.path}
        render={props => {
            if (route.hasOwnProperty('redirect')) {
                return (<Redirect
                    to={{
                        pathname: route.redirect
                    }}
                />)
            } else if (route.hasOwnProperty('protected')) {
                if (route.protected === 'private') {
                    return privateMiddleware(route, props);
                } else if (route.protected === 'service') {
                    return serviceSubscriptionMiddleware(route, props);
                }
            } else {
                // pass the sub-routes down to keep nesting
                return (<route.component {...props} routes={route.routes}/>)
            }
        }}
    />
);

// Middlewares
const privateMiddleware = (route, props) => {
    const {userData} = rootStore.user;
    if (userData.token.length) {
        return (<route.component {...props} routes={route.routes}/>)
    } else {
        return (<Redirect
            to={{
                pathname: '/public'
            }}
        />)
    }
};

const serviceSubscriptionMiddleware = (route, props) => {
    const {userServices} = rootStore.user;
    if (userServices.includes(route.service)) {
        return (<route.component {...props} routes={route.routes}/>)
    } else {
        return (<NoServiceContainer/>)
    }
};