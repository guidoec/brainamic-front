import React, {Component} from 'react';
// Styles
import './NoServiceContainer.scss'
// Components
import Typography from '@material-ui/core/Typography';

const NoServiceContainer = class NoServiceContainer extends Component {

    render() {
        return (
            <div className="no-service-container">
                <Typography gutterBottom variant="headline" component="h1">
                    No service in this subscription
                </Typography>
            </div>
        )
    }
}

export default NoServiceContainer;
